import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { WetherService } from './wether.service';
import { Chart } from 'chart.js';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  chart = [];
  @ViewChild('canvas') nativeChart: ElementRef;
  constructor(private weather: WetherService) {}

  ngOnInit() {
    this.weather.dailyForcast('Siedlce').subscribe(res => {
      console.log(res);
      const arr = res['list'].map(elm => {
        return {
          dt: new Date(elm.dt * 1000).toLocaleTimeString('pl', {
            day: 'numeric',
            month: 'short',
            year: 'numeric'
          }),
          main: elm.main,
          weather: elm.weather
        };
      });
      console.log(arr);
      this.chart = new Chart(this.nativeChart.nativeElement.getContext('2d'), {
        type: 'line',
        data: {
          labels: arr.map(elm => elm.dt),
          datasets: [
            {
              data: arr.map(elm => elm.main.temp_max),
              borderColor: '#3cba9f',
              fill: false
            },
            {
              data: arr.map(elm => elm.main.temp_min),
              borderColor: '#ffcc00',
              fill: false
            }
          ]
        },
        options: {
          legend: {
            display: false
          },
          scales: {
            xAxes: [
              {
                display: true
              }
            ],
            yAxes: [
              {
                display: true
              }
            ]
          }
        }
      });
    });
  }
}
