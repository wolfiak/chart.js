import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../environments/environment';

@Injectable()
export class WetherService {
  constructor(private http: HttpClient) {}

  dailyForcast(city: string) {
    return this.http
      .get(`${environment.endpoint}${city},pl&APPID=${environment.api}&units=metric`)
      .pipe(map(res => res));
  }
}
